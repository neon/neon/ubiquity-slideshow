#!/usr/bin/env ruby

require_relative '../releaseme/lib/releaseme/l10n'
require_relative '../releaseme/lib/releaseme/origin'
require_relative '../releaseme/lib/releaseme/project'

FileUtils.rm_r('po') if Dir.exist?('po')

#projects = ReleaseMe::Project.from_repo_url('ubiquity-slideshow-neon')
projects = ReleaseMe::Project.from_find('ubiquity-slideshow-neon')
raise "failed to resolve project #{projects}" unless projects.size == 1
project = projects[0]

l10n = ReleaseMe::L10n.new(ReleaseMe::Origin::TRUNK,
                            project.identifier, project.i18n_path)
l10n.get('potmp')
